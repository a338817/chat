# chat



## Getting started

para hacer funcionar este proyecto, hay que comenzar obteniendo el repositorio, una vez obtenido 2 veces en diferentes localizaciones.

## Agregar node_modules

Agregar los node_modules con los siguietes comandos:

```
cd chat
npm install

```

## Agregar la imagen de rabbit mq

- En nuestra terminal colocamos el siguiente comando:
```
docker run -dti -P --hostname=rabbit --name=rabbit -e RABBITMQ_DEFAULT_USER=eric -e RABBITMQ_DEFAULT_PASS=123456 rabbitmq:3-management

```
ejecutamos nuestro contenedor

## ejecutar el chat

- solo colocamos el siguiente comando en nuestro repositorio
```
node chatActive

```

y listo

