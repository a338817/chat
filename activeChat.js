const amqp = require('amqplib/callback_api');
const readline = require('readline');

const uri = "amqp://eric:123456@localhost:55005"



function obtenerNombreConsola(question) {
    return new Promise((resolve, reject) => {
      const readline = require('readline').createInterface({
        input: process.stdin,
        output: process.stdout
      });
      
      readline.question(question, (nombre) => {
        readline.close();
        if (nombre) {
          resolve(nombre);
        } else {
          reject(new Error('No se ingresó un nombre válido'));
        }
      });
    });
  }

async function crearConexion() {
    const name1Queue = await obtenerNombreConsola('Nombre del host: ');
    const name2Queue = await obtenerNombreConsola('Nombre adicional: ');
    const nameQueue = `${name1Queue}-${name2Queue}-chat`;
  amqp.connect(uri, (err, con) => {
    if (err) {
      throw err;
    }
    con.createChannel((errCh, channel) => {
      if (errCh) {
        throw errCh;
      }

      channel.assertQueue(nameQueue, {
        durable: false
      });

      console.log(`Bienvenido al chat`);

      channel.consume(nameQueue, (msg) => {
        console.log(`Mensaje recibido: ${msg.content.toString()}`);
      }, { noAck: true });

      // Leer mensajes desde la consola y enviarlos a la cola

    const rl = require('readline').createInterface({
        input: process.stdin,
        output: process.stdout
    });

      rl.on('line', (input) => {
        channel.sendToQueue(nameQueue, Buffer.from(input));
      });
    });
  });
}
crearConexion();